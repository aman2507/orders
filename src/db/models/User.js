const Mongoose = require('mongoose');

const schema = new Mongoose.Schema({
  userId: { type: String, require: true, trim: true },
  name: { type: String, default: '', trim: true },
  orderCount: { type: Number, trim: true, default: 0 },
})

module.exports = Mongoose.model('Users', schema);
