const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const schema = new Mongoose.Schema({
  userId: { type: Schema.Types.ObjectId, required: true, ref: 'Users' },
  orderId: { type: String, required: true },
  subTotal: { type: Number, required: true },
}, { timestamps: true });

module.exports = Mongoose.model('Order', schema);
