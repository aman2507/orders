const express = require('express');
const router = express.Router();
const aeh = require('../middleware/asyncErrorHandler');
const Order = require('../db/models/Order');
const User = require('../db/models/User');


// ============API to Create Order==============
router.post('/create', aeh(async function (req, res) {
  const { userId, orderId, subTotal } = req.body
  if (!userId || !orderId || !subTotal) {
    return res.status(404).error("Please pass userId,orderId and subTotal in body")
  }
  const userData = await User.find({ _id: userId })
  if (!userData || userData.length == 0) {
    return res.status(404).error("Error! User Not Found!!")
  }
  const orderData = await Order.create({ userId, orderId, subTotal });
  const updateOrderCount = await User.updateOne({ _id: userId }, { $inc: { orderCount: 1 } })
  return res.success("Order Created successfully", orderData)
}));

// =============API to GET all Orders================
router.get('/', aeh(async function (req, res) {
  const allOrders = await Order.find();
  return res.success("User found successfully", allOrders)
}));

//=============API to GET userwise Orders===============//
router.get('/users', aeh(async function (req, res) {
  const findOrder = await Order.aggregate([
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "name"
      }
    },
    { $unwind: "$name" },
    { $group: { "_id": "$userId", noOfOrder: { $sum: 1 }, averageBillValue: { $avg: "$subTotal" }, name: { $first: "$name.name" } } },
  ])
    .then(function (data) {
      return res.success("User found successfully", data)
    })
    .catch(function (err) { console.log(err) })
}));

module.exports = router;