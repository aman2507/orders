const express = require('express');
const router = express.Router();
const User = require('../db/models/User');
const aeh = require('../../src/middleware/asyncErrorHandler');

// ============API to Create User==============
router.post('/create', aeh(async function (req, res) {
  const { name, userId } = req.body
  if (!name || !userId) {
    return res.status(404).error("Please pass name and userId in body")
  }
  const allUsers = await User.create({ name, userId });
  return res.success("User Saved successfully", allUsers)
}));

// =============API to GET all users================
router.get('/', aeh(async function (req, res) {
  const allUsers = await User.find();
  return res.success("User found successfully", allUsers)
}));

module.exports = router;